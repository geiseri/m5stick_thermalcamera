#include <Adafruit_MLX90640.h>
#include <Ewma.h>
#include <M5StickCPlus.h>
#include <WiFi.h>
#include <array>
#include <vector>


TFT_eSprite Disbuff = TFT_eSprite(&M5.Lcd);

template <size_t R = 24, size_t C = 32>
struct camera
{
    static constexpr size_t rows = R;
    static constexpr size_t cols = C;
    std::array<float, R* C> frame = {{0}};
    Adafruit_MLX90640 mlx;

    bool begin()
    {
        if (mlx.begin(MLX90640_I2CADDR_DEFAULT, &Wire))
        {
            mlx.setMode(MLX90640_CHESS);
            mlx.setResolution(MLX90640_ADC_18BIT);
            mlx.setRefreshRate(MLX90640_16_HZ);
            Wire.setClock(1000000); // max 1 MHz
            return true;
        }
        return false;
    }

    bool capture() { return mlx.getFrame(frame.data()) == 0; }

    int get(int x, int y) { return frame[y * cols + x]; }
    std::pair<int, int> at(size_t index)
    {
        int x = index % cols;
        int y = int(index / cols);
        return std::pair<int, int>{x, y};
    }
};

struct temp_range
{
    int min;
    int max;
    std::string name;
};

struct color_range
{
    uint8_t min;
    uint8_t max;
};

std::vector<temp_range> ranges{
    temp_range{10, 32, "Wide"}, temp_range{5, 32, "Mammals"},    temp_range{20, 32, "Humans"},
    temp_range{20, 50, "Hot"},  temp_range{-10, 200, "Extreme"}, temp_range{0, 0, "Auto"},
};

// the colors we will be using
const uint16_t camColors[] = {
    0x480F, 0x400F, 0x400F, 0x400F, 0x4010, 0x3810, 0x3810, 0x3810, 0x3810, 0x3010, 0x3010, 0x3010,
    0x2810, 0x2810, 0x2810, 0x2810, 0x2010, 0x2010, 0x2010, 0x1810, 0x1810, 0x1811, 0x1811, 0x1011,
    0x1011, 0x1011, 0x0811, 0x0811, 0x0811, 0x0011, 0x0011, 0x0011, 0x0011, 0x0011, 0x0031, 0x0031,
    0x0051, 0x0072, 0x0072, 0x0092, 0x00B2, 0x00B2, 0x00D2, 0x00F2, 0x00F2, 0x0112, 0x0132, 0x0152,
    0x0152, 0x0172, 0x0192, 0x0192, 0x01B2, 0x01D2, 0x01F3, 0x01F3, 0x0213, 0x0233, 0x0253, 0x0253,
    0x0273, 0x0293, 0x02B3, 0x02D3, 0x02D3, 0x02F3, 0x0313, 0x0333, 0x0333, 0x0353, 0x0373, 0x0394,
    0x03B4, 0x03D4, 0x03D4, 0x03F4, 0x0414, 0x0434, 0x0454, 0x0474, 0x0474, 0x0494, 0x04B4, 0x04D4,
    0x04F4, 0x0514, 0x0534, 0x0534, 0x0554, 0x0554, 0x0574, 0x0574, 0x0573, 0x0573, 0x0573, 0x0572,
    0x0572, 0x0572, 0x0571, 0x0591, 0x0591, 0x0590, 0x0590, 0x058F, 0x058F, 0x058F, 0x058E, 0x05AE,
    0x05AE, 0x05AD, 0x05AD, 0x05AD, 0x05AC, 0x05AC, 0x05AB, 0x05CB, 0x05CB, 0x05CA, 0x05CA, 0x05CA,
    0x05C9, 0x05C9, 0x05C8, 0x05E8, 0x05E8, 0x05E7, 0x05E7, 0x05E6, 0x05E6, 0x05E6, 0x05E5, 0x05E5,
    0x0604, 0x0604, 0x0604, 0x0603, 0x0603, 0x0602, 0x0602, 0x0601, 0x0621, 0x0621, 0x0620, 0x0620,
    0x0620, 0x0620, 0x0E20, 0x0E20, 0x0E40, 0x1640, 0x1640, 0x1E40, 0x1E40, 0x2640, 0x2640, 0x2E40,
    0x2E60, 0x3660, 0x3660, 0x3E60, 0x3E60, 0x3E60, 0x4660, 0x4660, 0x4E60, 0x4E80, 0x5680, 0x5680,
    0x5E80, 0x5E80, 0x6680, 0x6680, 0x6E80, 0x6EA0, 0x76A0, 0x76A0, 0x7EA0, 0x7EA0, 0x86A0, 0x86A0,
    0x8EA0, 0x8EC0, 0x96C0, 0x96C0, 0x9EC0, 0x9EC0, 0xA6C0, 0xAEC0, 0xAEC0, 0xB6E0, 0xB6E0, 0xBEE0,
    0xBEE0, 0xC6E0, 0xC6E0, 0xCEE0, 0xCEE0, 0xD6E0, 0xD700, 0xDF00, 0xDEE0, 0xDEC0, 0xDEA0, 0xDE80,
    0xDE80, 0xE660, 0xE640, 0xE620, 0xE600, 0xE5E0, 0xE5C0, 0xE5A0, 0xE580, 0xE560, 0xE540, 0xE520,
    0xE500, 0xE4E0, 0xE4C0, 0xE4A0, 0xE480, 0xE460, 0xEC40, 0xEC20, 0xEC00, 0xEBE0, 0xEBC0, 0xEBA0,
    0xEB80, 0xEB60, 0xEB40, 0xEB20, 0xEB00, 0xEAE0, 0xEAC0, 0xEAA0, 0xEA80, 0xEA60, 0xEA40, 0xF220,
    0xF200, 0xF1E0, 0xF1C0, 0xF1A0, 0xF180, 0xF160, 0xF140, 0xF100, 0xF0E0, 0xF0C0, 0xF0A0, 0xF080,
    0xF060, 0xF040, 0xF020, 0xF800,
};

uint16_t displayPixelWidth, displayPixelHeight;

float temp = 0;

bool check_pressed(Button* btn)
{
    M5.update();
    bool pressed = false;
    while (btn->isPressed())
    {
        M5.update();
        M5.Beep.tone(4000);
        delay(10);
        pressed = true;
    }
    M5.Beep.mute();
    return pressed;
}

bool configure_wifi()
{
    M5.Lcd.println("Wireless SmartConfig...");
    WiFi.mode(WIFI_AP_STA);
    WiFi.beginSmartConfig();
    while (!WiFi.smartConfigDone())
    {
        if (check_pressed(&M5.BtnA))
        {
            M5.Lcd.println("Canceled");
            WiFi.stopSmartConfig();
            return false;
        }
        delay(500);
    }
    M5.Lcd.println("Connecting...");
    for (int count = 0; count < 30; count++)
    {
        if (WiFi.status() == WL_CONNECTED) return true;
        if (check_pressed(&M5.BtnA))
        {
            M5.Lcd.println("Canceled");
            return false;
        }
        delay(500);
    }
    M5.Lcd.println("Timeout!");
    return false;
}

void checkAXPPress()
{
    if (M5.Axp.GetBtnPress())
    {
        do {
            delay(20);
        } while (M5.Axp.GetBtnPress());
        M5.Beep.mute();
        ESP.restart();
    }
}

void ErrorDialog(uint8_t code, const char* str)
{
    Disbuff.fillRect(28, 20, 184, 95, Disbuff.color565(45, 45, 45));
    Disbuff.fillRect(30, 22, 180, 91, TFT_BLACK);
    Disbuff.drawRect(40, 43, 48, 48, TFT_RED);

    Disbuff.setCursor(145, 37);
    Disbuff.setTextFont(2);
    Disbuff.printf("%02X", code);
    Disbuff.drawString("ERROR", 55 + 45, 10 + 27, 2);
    Disbuff.drawString("-----------------", 55 + 45, 30 + 27, 1);
    Disbuff.drawString(str, 55 + 45, 45 + 27, 1);
    Disbuff.drawString("check Hardware ", 55 + 45, 60 + 27, 1);
    Disbuff.pushSprite(0, 0);

    while ((!M5.BtnA.isPressed()) && (!M5.BtnB.isPressed()))
    {
        M5.update();
        checkAXPPress();
        delay(100);
    }
    while ((M5.BtnA.isPressed()) || (M5.BtnB.isPressed()))
    {
        M5.update();
        checkAXPPress();
        M5.Beep.tone(4000);
        delay(10);
    }
    delay(50);
    M5.Beep.mute();
    Disbuff.setTextColor(TFT_WHITE);
    Disbuff.setTextFont(1);
}

camera<24, 32> mlx_;

void setup()
{
    M5.begin();
    M5.Axp.ScreenBreath(10);
    M5.Lcd.setRotation(1);
    Disbuff.createSprite(240, 135);
    Disbuff.setRotation(1);
    Disbuff.fillSprite(BLACK);
    Disbuff.pushSprite(0, 0);

    displayPixelWidth = 160 / mlx_.cols;
    displayPixelHeight = 160 / mlx_.cols; // Keep pixels square

    delay(100);

    if (configure_wifi()) { M5.Lcd.println("Testing api..."); }

    if (!mlx_.begin())
    {
        Serial.println("Failed");
        ErrorDialog(0x22, "MLX90640 not found!");
    }
    M5.Lcd.println("Found Adafruit MLX90640");
}

int current_range = 0;
Ewma filterMin(0.05);
Ewma filterMax(0.1);
Ewma filterFPS(0.1);

void loop()
{
    uint32_t timestamp = millis();
    color_range cr{0, 255};

    if (check_pressed(&M5.BtnA)) { current_range++; }

    if (!mlx_.capture()) { ErrorDialog(0x22, "MLX90640 failed to read"); }

    Disbuff.fillSprite(BLACK);
    temp_range& range = ranges[current_range % ranges.size()];
    int mintemp = range.min;
    int maxtemp = range.max;
    std::string title = range.name;
    if (title == "Auto")
    {
        auto delta = (maxtemp - mintemp) * 0.1f; // 10%
        mintemp -= delta;
        maxtemp += delta;
    }
    int currmin = mlx_.frame[0];
    int currmax = mlx_.frame[0];
    auto width = displayPixelWidth * mlx_.cols;
    auto height = displayPixelHeight * mlx_.rows;
    for (uint8_t h = 0; h < mlx_.rows; ++h)
    {
        for (uint8_t w = 0; w < mlx_.cols; w++)
        {
            int t = mlx_.get(w, h);

            uint8_t colorIndex = map(constrain(t, mintemp, maxtemp), mintemp, maxtemp, 0, 255);
            colorIndex = constrain(colorIndex, 0, 255);
            if (t > currmax)
            {
                currmax = t;
                cr.max = colorIndex;
            }

            if (t < currmin)
            {
                currmin = t;
                cr.min = colorIndex;
            }
            // draw the pixels!
            Disbuff.fillRect(width - displayPixelWidth * w, displayPixelHeight * h,
                             displayPixelHeight, displayPixelWidth, camColors[colorIndex]);
        }
    }

    ranges.back().min = filterMin.filter(currmin) - 1;
    ranges.back().max = filterMax.filter(currmax) + 1;

    for (int x = displayPixelWidth; x < width; x++)
    {
        uint8_t colorIndex = map(x, 0, width, cr.min, cr.max);
        Disbuff.drawLine(x, height, x, 135, camColors[colorIndex]);
    }

    uint8_t min_col = map(mintemp, mintemp, maxtemp, 0, 255);
    uint8_t max_col = map(maxtemp, mintemp, maxtemp, 0, 255);
    for (int y = 0; y < height; y++)
    {
        uint8_t colorIndex = map(y, 0, height, min_col, max_col);
        Disbuff.drawLine(width, y, width + 15, y, camColors[colorIndex]);
    }
    Disbuff.setTextFont(1);
    Disbuff.setCursor(width + 20, 0);
    Disbuff.printf("%dC", mintemp);
    Disbuff.setCursor(width + 20, height - 8);
    Disbuff.printf("%dC", maxtemp);

    float vBat = M5.Axp.GetBatVoltage();
    uint16_t vBat_color = TFT_GREEN;
    if (vBat < 3.6) { vBat_color = TFT_YELLOW; }
    if (vBat < 3.4) { vBat_color = TFT_ORANGE; }
    if (vBat < 3.2) { vBat_color = TFT_RED; }
    Disbuff.fillRect(185, 20, 240, 20, vBat_color);
    Disbuff.setCursor(190, 20);
    Disbuff.setTextFont(2);
    Disbuff.setTextColor(TFT_BLACK);
    Disbuff.printf("B %.1fV", vBat);
    Disbuff.setTextColor(TFT_WHITE);
    Disbuff.setTextFont(2);
    Disbuff.setCursor(190, 40);
    Disbuff.printf("%s", title.c_str());
    Disbuff.setCursor(190, 60);
    Disbuff.printf("H: %dC", currmax);
    Disbuff.setCursor(190, 80);
    Disbuff.printf("L: %dC", currmin);

    Disbuff.pushSprite(0, 0);
    M5.update();

    uint32_t delta = filterFPS.filter(millis() - timestamp);
    if (millis() % 100000)
    {
        Serial.printf("FPS: %.2f\n", 1000.0f / delta);
        filterFPS.reset();
    }
}
