#include "spdlog/cfg/env.h"
#include <filesystem>
#include <pngwriter.h>
#include <restinio/all.hpp>

namespace rr = restinio::router;
using router_t = rr::express_router_t<>;

struct server_config
{
    std::filesystem::path savedir;
};

template <typename RESP>
RESP init_resp(RESP resp)
{
    resp.append_header(restinio::http_field::server, "RESTinio sample server /v.0.2");
    resp.append_header_date_field();
    return resp;
}

auto server_handler(const server_config& cfg)
{
    auto router = std::make_unique<router_t>();

    auto method_not_allowed = [](const auto& req, const auto&) {
        return req->create_response(restinio::status_method_not_allowed())
            .connection_close()
            .done();
    };

    // Handlers for '/' path.
    router->http_post("/snapshot", [](auto req, auto) {
        auto resp = init_resp(req->create_response());
        try
        {
            req->body();
            resp.append_header(restinio::http_field::content_type, "text/plain; charset=utf-8")
                .set_body("OK")
                .done();

        } catch (const std::exception& e)
        {
            resp.header().status_line(restinio::status_bad_request());
        }
        return restinio::request_accepted();
    });

    router->non_matched_request_handler([](auto req) {
        return req->create_response(restinio::status_not_found())
            .append_header_date_field()
            .connection_close()
            .done();
    });

    return router;
}

int main(int argc, char** argv)
{
    using namespace std::chrono;

    spdlog::cfg::load_env_levels();
    spdlog::set_pattern("[%H:%M:%S %z] [%n] [%^---%L---%$] [thread %t] %v");
    server_config cfg{"."};
    try
    {
        using traits_t = restinio::traits_t<restinio::asio_timer_manager_t,
                                            restinio::single_threaded_ostream_logger_t, router_t>;
        restinio::run(restinio::on_this_thread<traits_t>()
                          .address("localhost")
                          .request_handler(server_handler(cfg))
                          .read_next_http_message_timelimit(10s)
                          .write_http_response_timelimit(1s)
                          .handle_request_timeout(1s));
    } catch (const std::exception& e)
    {
        spdlog::error("Exception: {}", e.what());
    }
    return 0;
}
